# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/06 14:25:26 by hmerieux          #+#    #+#              #
#    Updated: 2019/09/17 17:58:28 by hmerieux         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CPPFLAGS = -Iincludes -Ilibft/includes

PATH_SRC = src/
PATH_OBJ = obj/
PATH_LIB = libft/

SRC_NAME =	ft_printf.c \
			convert_char.c convert_float.c convert_octal.c convert_string.c convert_digit.c \
			convert_hexal.c convert_pointeur.c calcul_float.c\
			init_struct.c tools.c utools.c bonus.c\

OBJ = $(SRC_NAME:%.c=$(PATH_OBJ)%.o)

SRC = $(addprefix $(PATH_SRC), $(SRC_NAME))
LIB = $(addprefix $(PATH_LIB),libft.a)

CC = gcc

DEBUG = -g3 -fsanitize=address

CFLAGS = -Wall -Werror -Wextra

all: $(NAME)


$(NAME): $(LIB) $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)
	printf "\t\t\tLibPrintf.a is ready\n"

$(LIB):
	make -C $(PATH_LIB)
	cp $(LIB) ./$(NAME)

$(PATH_OBJ)%.o: $(PATH_SRC)%.c
	mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $^

clean:
	rm -rf obj
	@make clean -C $(PATH_LIB)


fclean: clean
	rm -f $(NAME)
	make fclean -C $(PATH_LIB)

re: fclean all

norme:
	norminette $(PATH_LIB)
	norminette $(SRC)
	norminette includes/ft_printf.h

.PHONY: all, clean, fclean, re, norme
.SILENT:
