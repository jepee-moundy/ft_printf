/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/08 15:00:38 by hmerieux          #+#    #+#             */
/*   Updated: 2019/07/08 15:39:37 by hmerieux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	ft_checkopt(char av, char *flags)
{
	while (*flags)
	{
		if (*flags == av)
			return (av);
		flags++;
	}
	return ('?');
}

int		ft_getopt(int *ac, char **const av, char *flags)
{
	static int	i = 0;
	char		option;

	if (av[*ac] && (i != 0 || (av[*ac][i++] == '-')))
	{
		if (av[*ac][i] == '-')
		{
			*ac += 1;
			return (-1);
		}
		if (((option = ft_checkopt(av[*ac][i], flags)) == '?')
				|| av[*ac][++i])
			return (option);
		i = 0;
		*ac += 1;
		return (option);
	}
	return (-1);
}
