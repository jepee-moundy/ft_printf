/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 16:56:37 by hmerieux          #+#    #+#             */
/*   Updated: 2018/11/30 12:34:38 by hmerieux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strjoin(char const *s, char const *s2)
{
	char	*dst;

	if (s == NULL)
		return ((char *)s2);
	if (s2 == NULL)
		return ((char *)s);
	dst = ft_strnew(ft_strlen(s) + ft_strlen(s2));
	if (!dst)
		return (NULL);
	ft_strcat(dst, s);
	ft_strcat(dst, s2);
	return (dst);
}
