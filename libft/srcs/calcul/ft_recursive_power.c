/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 17:01:00 by hmerieux          #+#    #+#             */
/*   Updated: 2018/11/30 17:07:58 by hmerieux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_power(int nb, int pw)
{
	if (pw < 0)
		return (0);
	else if (pw == 0)
		return (1);
	else
		return (ft_recursive_power(nb, pw - 1) * nb);
}
