/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 16:36:41 by hmerieux          #+#    #+#             */
/*   Updated: 2018/11/29 16:41:36 by hmerieux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	void	*s;
	int		i;

	i = 0;
	s = (void *)malloc(sizeof(s) * (int)size);
	if (!s)
		return (NULL);
	while (i < (int)size)
	{
		((int *)s)[i] = 0;
		i++;
	}
	return (s);
}
