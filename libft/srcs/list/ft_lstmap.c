/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmerieux <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 19:51:45 by hmerieux          #+#    #+#             */
/*   Updated: 2018/11/29 16:38:58 by hmerieux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*lstdst;
	t_list	*begin;
	t_list	*tmp;

	tmp = f(lst);
	lstdst = ft_lstnew(tmp->content, tmp->content_size);
	if (!lstdst)
		return (NULL);
	begin = lstdst;
	lst = lst->next;
	while (lst)
	{
		tmp = f(lst);
		lstdst->next = ft_lstnew(tmp->content, tmp->content_size);
		if (!lstdst)
			return (NULL);
		lstdst = lstdst->next;
		lst = lst->next;
	}
	return (begin);
}
